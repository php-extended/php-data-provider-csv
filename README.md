# php-extended/php-data-provider-csv

An implementation of php-extended/php-data-provider-csv that interprets csv data.

![coverage](https://gitlab.com/php-extended/php-data-provider-csv/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-data-provider-csv/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-data-provider-csv ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\DataProvider\CsvFileDataProvider;

$provider = new CsvFileDataProvider('/path/to/csv.file', true);

foreach($provider->provideIterator() as $line => $data)
{
	// do something
}

```

You may also use an in-memory data provider with a string as raw csv data using
the `PhpExtended\DataProvider\CsvStringDataProvider`.


## License

MIT (See [license file](LICENSE)).
