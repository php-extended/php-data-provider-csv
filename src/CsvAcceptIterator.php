<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use FilterIterator;
use Iterator;
use Stringable;

/**
 * CsvAcceptIterator class file.
 * 
 * This class is a filter iterator that accepts lines given a callback.
 * 
 * @author Anastaszor
 * @extends \FilterIterator<integer, array<string>, \Iterator<integer, array<string>>>
 */
class CsvAcceptIterator extends FilterIterator implements Stringable
{
	
	/**
	 * Thenull|callable to check whether to accept the given data line.
	 *
	 * @var null|callable(array<integer, null|integer|float|string>):bool
	 */
	protected $_acceptLine;
	
	/**
	 * Builds a new CsvAcceptIterator with the given parent iterator and accept
	 * callback.
	 * 
	 * @param Iterator<integer, array<string>> $iterator
	 * @param null|callable(array<integer, null|integer|float|string>):bool $acceptLine
	 */
	public function __construct(Iterator $iterator, ?callable $acceptLine)
	{
		parent::__construct($iterator);
		$this->_acceptLine = $acceptLine;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \FilterIterator::accept()
	 */
	public function accept() : bool
	{
		$callable = $this->_acceptLine;
		if(null === $callable)
		{
			return true;
		}
		
		$current = (array) parent::current();
		
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return (bool) $callable($current);
	}
	
}
