<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use RuntimeException;
use Throwable;

/**
 * UnprovidableCsvException class file.
 * 
 * This exception is a simple implementation of UnprovidableThrowable for this
 * library.
 * 
 * @author Anastaszor
 */
class UnprovidableCsvException extends RuntimeException implements UnprovidableThrowable
{
	
	/**
	 * The source from which the providing failed.
	 * 
	 * @var string
	 */
	protected string $_source;
	
	/**
	 * The index when the providing failed.
	 * 
	 * @var integer
	 */
	protected int $_index;
	
	/**
	 * Builds a new UnprovidedCsvException with the given data.
	 * 
	 * @param string $source
	 * @param integer $index
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 */
	public function __construct(string $source, int $index, string $message, int $code = -1, ?Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->_source = $source;
		$this->_index = $index;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\UnprovidableThrowable::getSource()
	 */
	public function getSource() : string
	{
		return $this->_source;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\UnprovidableThrowable::getIndex()
	 */
	public function getIndex() : int
	{
		return $this->_index;
	}
	
}
