<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use Iterator;
use Stringable;

/**
 * CsvStringDataIterator class file.
 * 
 * This class is an iterator over an in-memory data string.
 * 
 * @author Anastaszor
 * @implements \Iterator<integer, array<string>>
 */
class CsvStringDataIterator implements Iterator, Stringable
{
	
	/**
	 * Whether to skip the first line of the csv file.
	 *
	 * @var boolean
	 */
	protected bool $_skipHeader;
	
	/**
	 * The character used to separate fields.
	 *
	 * @var string
	 */
	protected string $_delimiter;
	
	/**
	 * The character used to enclose values in fields.
	 *
	 * @var string
	 */
	protected string $_enclosure;
	
	/**
	 * The character used to escape values in fields.
	 *
	 * @var string
	 */
	protected string $_escaper;
	
	/**
	 * The pointer to the file.
	 *
	 * @var resource
	 */
	protected $_fpointer;
	
	/**
	 * The length of the stream.
	 * 
	 * @var integer
	 */
	protected int $_length = 0;
	
	/**
	 * The current row count.
	 *
	 * @var integer
	 */
	protected int $_row = 0;
	
	/**
	 * The current row data.
	 *
	 * @var array<integer|string, string>
	 */
	protected array $_current = [];
	
	/**
	 * The source encoding.
	 *
	 * @var string
	 */
	protected string $_fromEncoding;
	
	/**
	 * The destination encoding.
	 *
	 * @var string
	 */
	protected string $_toEncoding;
	
	/**
	 * The line of the headers, if any.
	 *
	 * @var array<integer, string>
	 */
	private array $_headerLine = [];
	
	/**
	 * Builds a new CsvFileDataIterator with the given path and header status.
	 *
	 * @param string $data
	 * @param boolean $skipHeader
	 * @param string $delimiter
	 * @param string $enclosure
	 * @param string $escaper
	 * @param string $fromEncoding
	 * @param string $toEncoding
	 * @throws UnprovidableCsvException
	 */
	public function __construct(
		string $data,
		bool $skipHeader,
		string $delimiter = ',',
		string $enclosure = '"',
		string $escaper = '\\',
		string $fromEncoding = 'UTF-8',
		string $toEncoding = 'UTF-8'
	) {
		$this->_skipHeader = $skipHeader;
		$this->_delimiter = $delimiter;
		$this->_enclosure = $enclosure;
		$this->_escaper = $escaper;
		$this->_length = (int) \mb_strlen($data);
		$this->_fromEncoding = $fromEncoding;
		$this->_toEncoding = $toEncoding;
		
		// use a temp memory pointer to store the stream and treat it like a
		// file ; do this because str_getcsv is bugged if fields contains lf
		// https://www.php.net/manual/en/function.str-getcsv.php
		$fopen = \fopen('php://memory', 'rw');
		// @codeCoverageIgnoreStart
		if(false === $fopen)
		{
			$message = 'Failed to create memory stream of length {l}, maybe you run out of available memory ?';
			$context = ['{l}' => $this->_length];
			
			throw new UnprovidableCsvException('php://memory', 0, \strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
		
		$this->_fpointer = $fopen;
		$stt = \fwrite($this->_fpointer, $data);
		// @codeCoverageIgnoreStart
		if(false === $stt)
		{
			$message = 'Failed to write to memory stream length {l}, maybe you run out of available memory ?';
			$context = ['{l}' => $this->_length];
			
			throw new UnprovidableCsvException('php://memory', 0, \strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@string('.((string) $this->_length).')';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 */
	public function current() : array
	{
		return $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 * @throws UnprovidableCsvException
	 */
	public function next() : void
	{
		$this->_row++;
		
		$nextrow = [null];
		
		while($nextrow === [null])
		{
			// Note: A blank line in a CSV file will be returned as an array
			// comprising a single null field, and will not be treated as an error.
			$nextrow = \fgetcsv($this->_fpointer, 0, $this->_delimiter, $this->_enclosure, $this->_escaper);
		}
		
		if(empty($nextrow))
		{
			// assume end of stream
			$this->_current = [];
			
			return;
		}
		
		$newrow = [];
		
		foreach($nextrow as $k => $value)
		{
			$newrow[(int) $k] = (string) $value;
		}
		
		$nextrow = $newrow;
		
		if($this->_toEncoding !== $this->_fromEncoding)
		{
			$newrow = [];
			
			foreach($nextrow as $k => $value)
			{
				$newrow[(int) $k] = (string) \mb_convert_encoding($value, $this->_toEncoding, $this->_fromEncoding);
			}
			
			$nextrow = $newrow;
		}
		
		if([] !== $this->_headerLine)
		{
			$formattedRow = [];
			
			foreach($nextrow as $idx => $value)
			{
				$formattedRow[$this->_headerLine[$idx] ?? $idx] = $value;
			}
			
			$nextrow = $formattedRow;
		}
		
		$this->_current = $nextrow;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return $this->_row;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return [] !== $this->_current;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 * @throws UnprovidableCsvException
	 */
	public function rewind() : void
	{
		\rewind($this->_fpointer);
		
		$this->next();
		if($this->_skipHeader)
		{
			$currentLine = $this->_current;
			if([] !== $currentLine)
			{
				$this->_headerLine = \array_map(function(string $data) : string
				{
					$data = (string) \mb_convert_encoding($data, $this->_toEncoding, $this->_fromEncoding);

					if('UTF-8' === $this->_toEncoding)
					{
						// remove all invalid utf8 chars
						$data = (string) \preg_replace('/\\p{Cc}+/u', '', $data);
						$data = (string) \str_replace("\xef\xbb\xbf", '', $data); // BOM
					}

					return $data;
				}, \array_values($currentLine));
			}
			$this->next();
		}
		
		$this->_row = 0;
	}
	
}
