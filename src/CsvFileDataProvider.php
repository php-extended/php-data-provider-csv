<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataProvider;

use Iterator;

/**
 * CsvFileDataProvider class file.
 * 
 * This class provides csv data based on a local data file.
 * 
 * @author Anastaszor
 */
class CsvFileDataProvider implements DataProviderInterface
{
	
	/**
	 * The full path of the file.
	 * 
	 * @var string
	 */
	protected string $_filepath;
	
	/**
	 * Whether to skip the first line of the csv file.
	 * 
	 * @var boolean
	 */
	protected bool $_skipHeader;
	
	/**
	 * The character used to separate fields.
	 * 
	 * @var string
	 */
	protected string $_delimiter;
	
	/**
	 * The character used to enclose values in fields.
	 * 
	 * @var string
	 */
	protected string $_enclosure;
	
	/**
	 * The character used to escape values in fields.
	 * 
	 * @var string
	 */
	protected string $_escaper;
	
	/**
	 * The source encoding.
	 * 
	 * @var string
	 */
	protected string $_fromEncoding;
	
	/**
	 * The destination encoding.
	 * 
	 * @var string
	 */
	protected string $_toEncoding;
	
	/**
	 * Thenull|callable to check whether to accept the given data line.
	 *
	 * @var null|callable(array<integer, null|integer|float|string>):bool
	 */
	protected $_acceptLine;
	
	/**
	 * Builds a new CsvFileDataProvider with the given string path name.
	 * 
	 * @param string $filename
	 * @param boolean $skipHeader
	 * @param string $delimiter
	 * @param string $enclosure
	 * @param string $escaper
	 * @param string $fromEncoding
	 * @param string $toEncoding
	 * @param null|callable(array<integer, null|integer|float|string>):bool $acceptLine
	 * @throws UnprovidableCsvException
	 */
	public function __construct(
		string $filename,
		bool $skipHeader,
		string $delimiter = ',',
		string $enclosure = '"',
		string $escaper = '\\',
		string $fromEncoding = 'UTF-8',
		string $toEncoding = 'UTF-8',
		?callable $acceptLine = null
	) {
		$realpath = \realpath($filename);
		if(false === $realpath)
		{
			$message = 'No objects at {path} can be found by realpath';
			$context = ['{path}' => $filename];
			
			throw new UnprovidableCsvException($filename, 0, \strtr($message, $context));
		}
		
		if(!\is_file($realpath))
		{
			$message = 'The file at {path} does not exists';
			$context = ['{path}' => $realpath];
			
			throw new UnprovidableCsvException($realpath, 0, \strtr($message, $context));
		}
		
		// @codeCoverageIgnoreStart
		if(!\is_readable($realpath))
		{
			$message = 'The file at {path} is not readable';
			$context = ['{path}' => $realpath];
			
			throw new UnprovidableCsvException($realpath, 0, \strtr($message, $context));
		}
		// @codeCoverageIgnoreEnd
		
		$this->_filepath = $realpath;
		$this->_skipHeader = $skipHeader;
		$this->_delimiter = $delimiter;
		$this->_enclosure = $enclosure;
		$this->_escaper = $escaper;
		$this->_fromEncoding = $fromEncoding;
		$this->_toEncoding = $toEncoding;
		$this->_acceptLine = $acceptLine;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@"file://'.$this->_filepath.'"';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::getSource()
	 */
	public function getSource() : string
	{
		return 'file://'.$this->_filepath;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::hasUnique()
	 */
	public function hasUnique() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideOne()
	 * @throws UnprovidableCsvException
	 */
	public function provideOne() : array
	{
		foreach($this->provideIterator() as $value)
		{
			return $value;
		}
		
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideAll()
	 * @throws UnprovidableCsvException
	 */
	public function provideAll() : array
	{
		$datas = [];
		
		foreach($this->provideIterator() as $value)
		{
			$datas[] = $value;
		}
		
		return $datas;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DataProvider\DataProviderInterface::provideIterator()
	 * @throws UnprovidableCsvException
	 */
	public function provideIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(
			$this->_filepath,
			$this->_skipHeader,
			$this->_delimiter,
			$this->_enclosure,
			$this->_escaper,
			$this->_fromEncoding,
			$this->_toEncoding,
		);
		
		if(null !== $this->_acceptLine)
		{
			$iterator = new CsvAcceptIterator($iterator, $this->_acceptLine);
		}
		
		return $iterator;
	}
	
}
