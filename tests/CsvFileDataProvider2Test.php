<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvFileDataProvider;
use PHPUnit\Framework\TestCase;

/**
 * CsvFileDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvFileDataIterator
 * @covers \PhpExtended\DataProvider\CsvFileDataProvider
 *
 * @internal
 *
 * @small
 */
class CsvFileDataProvider2Test extends TestCase
{
	
	/**
	 * The provider.
	 *
	 * @var CsvFileDataProvider
	 */
	protected CsvFileDataProvider $_object;
	
	public function testToString() : void
	{
		$object = $this->_object;
		$this->assertEquals(\get_class($object).'@"file://'.__DIR__.'/bomdata.csv"', $object->__toString());
	}

	
	public function testProvideAll() : void
	{
		$count = 0;

		foreach($this->_object->provideAll() as $object)
		{
			$this->assertEquals([
				'id' => '361S0361000005',
				'id_ex' => '0361000005',
				'gestion' => '361S',
			], $object);
			$count++;
		}

		$this->assertEquals(1, $count);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvFileDataProvider(
			__DIR__.'/bomdata.csv',
			true,
			';',
			'"',
			'\\',
			'UTF-8',
			'UTF-8',
		);
	}
	
}
