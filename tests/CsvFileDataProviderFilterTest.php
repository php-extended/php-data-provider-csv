<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvFileDataProvider;
use PhpExtended\DataProvider\UnprovidableCsvException;
use PHPUnit\Framework\TestCase;

/**
 * CsvFileDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvFileDataIterator
 * @covers \PhpExtended\DataProvider\CsvFileDataProvider
 *
 * @internal
 *
 * @small
 */
class CsvFileDataProviderFilterTest extends TestCase
{
	
	/**
	 * The provider.
	 *
	 * @var CsvFileDataProvider
	 */
	protected CsvFileDataProvider $_object;
	
	public function testSuccess() : void
	{
		$data = [
			['Header' => 'Header 1', 'Column' => 'Value 1'],
		];
		
		$this->assertEquals($data, $this->_object->provideAll());
	}
	
	public function testNotATarget() : void
	{
		$this->expectException(UnprovidableCsvException::class);
		
		new CsvFileDataProvider(__DIR__.'/foobar', true);
	}
	
	public function testNotAFile() : void
	{
		$this->expectException(UnprovidableCsvException::class);
		
		new CsvFileDataProvider(__DIR__, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvFileDataProvider(
			__DIR__.'/testdata.csv',
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
			function(array $data) : bool
			{
				return 'Header 1' === $data['Header'];
			},
		);
	}
	
}
