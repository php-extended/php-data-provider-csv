<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\UnprovidableCsvException;
use PHPUnit\Framework\TestCase;

/**
 * UnprovidableCsvExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\UnprovidableCsvException
 *
 * @internal
 *
 * @small
 */
class UnprovidableCsvExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var UnprovidableCsvException
	 */
	protected UnprovidableCsvException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(UnprovidableCsvException::class, $this->_object->__toString());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals('php://memory', $this->_object->getSource());
	}
	
	public function testGetIndex() : void
	{
		$this->assertEquals(10, $this->_object->getIndex());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new UnprovidableCsvException('php://memory', 10, 'message');
	}
	
}
