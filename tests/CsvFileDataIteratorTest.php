<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvFileDataIterator;
use PHPUnit\Framework\TestCase;

/**
 * CsvFileDataIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvFileDataIterator
 *
 * @internal
 *
 * @small
 */
class CsvFileDataIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CsvFileDataIterator
	 */
	protected CsvFileDataIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@"file://'.__DIR__.'/testdata.csv"', $this->_object->__toString());
	}
	
	public function testIterator() : void
	{
		foreach($this->_object as $key => $value)
		{
			$this->assertNotNull($key);
			$this->assertIsArray($value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvFileDataIterator(
			__DIR__.'/testdata.csv',
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
		);
	}
	
}
