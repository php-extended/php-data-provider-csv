<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvStringDataProvider;
use PHPUnit\Framework\TestCase;

/**
 * CsvStringDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvStringDataIterator
 * @covers \PhpExtended\DataProvider\CsvStringDataProvider
 *
 * @internal
 *
 * @small
 */
class CsvStringDataProviderFilterTest extends TestCase
{
	
	/**
	 * The provider.
	 * 
	 * @var CsvStringDataProvider
	 */
	protected CsvStringDataProvider $_object;
	
	public function testSuccess() : void
	{
		$data = [
			['Header' => 'Header 1', 'Column' => 'Value 1'],
		];
		
		$this->assertEquals($data, $this->_object->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvStringDataProvider(
			\file_get_contents(__DIR__.'/testdata.csv'),
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
			function(array $data) : bool
			{
				return 'Header 1' === $data['Header'];
			},
		);
	}
	
}
