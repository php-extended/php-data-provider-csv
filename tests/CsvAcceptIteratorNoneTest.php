<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvAcceptIterator;
use PHPUnit\Framework\TestCase;

/**
 * CsvAcceptIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvAcceptIterator
 *
 * @internal
 *
 * @small
 */
class CsvAcceptIteratorNoneTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CsvAcceptIterator
	 */
	protected CsvAcceptIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$k = 0;
		
		foreach($this->_object as $data)
		{
			$this->assertNotEmpty($data);
			$k++;
		}
		
		$this->assertEquals(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvAcceptIterator(new ArrayIterator([0 => 'data']), null);
	}
	
}
