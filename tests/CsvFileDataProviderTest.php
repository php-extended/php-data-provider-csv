<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvFileDataProvider;
use PhpExtended\DataProvider\UnprovidableCsvException;
use PHPUnit\Framework\TestCase;

/**
 * CsvFileDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvFileDataIterator
 * @covers \PhpExtended\DataProvider\CsvFileDataProvider
 *
 * @internal
 *
 * @small
 */
class CsvFileDataProviderTest extends TestCase
{
	
	/**
	 * The provider.
	 *
	 * @var CsvFileDataProvider
	 */
	protected CsvFileDataProvider $_object;
	
	public function testToString() : void
	{
		$object = $this->_object;
		$this->assertEquals(\get_class($object).'@"file://'.__DIR__.'/testdata.csv"', $object->__toString());
	}
	
	public function testToString2() : void
	{
		$object = $this->_object->provideIterator();
		$this->assertEquals(\get_class($object).'@"file://'.__DIR__.'/testdata.csv"', $object->__toString());
	}
	
	public function testHasMultiple() : void
	{
		$this->assertFalse($this->_object->hasUnique());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals('file://'.__DIR__.'/testdata.csv', $this->_object->getSource());
	}
	
	public function testProvideOne() : void
	{
		$expected = ['Header' => 'Header 1', 'Column' => 'Value 1'];
		
		$this->assertEquals($expected, $this->_object->provideOne());
	}
	
	public function testProvideOneEmpty() : void
	{
		$provider = new CsvFileDataProvider(__DIR__.'/emptydata.csv', true);
		$this->assertEquals([], $provider->provideOne());
	}
	
	public function testProvideAll() : void
	{
		$data = [
			['Header' => 'Header 1', 'Column' => 'Value 1'],
			['Header' => 'Header 2', 'Column' => 'Value 2'],
		];
		
		$this->assertEquals($data, $this->_object->provideAll());
	}
	
	public function testNotATarget() : void
	{
		$this->expectException(UnprovidableCsvException::class);
		
		new CsvFileDataProvider(__DIR__.'/foobar', true);
	}
	
	public function testNotAFile() : void
	{
		$this->expectException(UnprovidableCsvException::class);
		
		new CsvFileDataProvider(__DIR__, true);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvFileDataProvider(
			__DIR__.'/testdata.csv',
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
		);
	}
	
}
