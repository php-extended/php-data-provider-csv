<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvStringDataIterator;
use PHPUnit\Framework\TestCase;

/**
 * CsvStringDataIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvStringDataIterator
 *
 * @internal
 *
 * @small
 */
class CsvStringDataIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var CsvStringDataIterator
	 */
	protected CsvStringDataIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@string('.\mb_strlen(\file_get_contents(__DIR__.'/testdata.csv')).')', $this->_object->__toString());
	}
	
	public function testIterator() : void
	{
		foreach($this->_object as $key => $value)
		{
			$this->assertNotNull($key);
			$this->assertIsArray($value);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvStringDataIterator(
			\file_get_contents(__DIR__.'/testdata.csv'),
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
		);
	}
	
}
