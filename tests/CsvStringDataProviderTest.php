<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-provider-csv library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DataProvider\CsvStringDataProvider;
use PHPUnit\Framework\TestCase;

/**
 * CsvStringDataProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DataProvider\CsvStringDataIterator
 * @covers \PhpExtended\DataProvider\CsvStringDataProvider
 *
 * @internal
 *
 * @small
 */
class CsvStringDataProviderTest extends TestCase
{
	
	/**
	 * The provider.
	 * 
	 * @var CsvStringDataProvider
	 */
	protected CsvStringDataProvider $_object;
	
	public function testToString() : void
	{
		$object = $this->_object;
		$this->assertEquals(\get_class($object).'@string('.\mb_strlen(\file_get_contents(__DIR__.'/testdata.csv')).')', $object->__toString());
	}
	
	public function testToString2() : void
	{
		$object = $this->_object->provideIterator();
		$this->assertEquals(\get_class($object).'@string('.\mb_strlen(\file_get_contents(__DIR__.'/testdata.csv')).')', $object->__toString());
	}
	
	public function testHasMultiple() : void
	{
		$this->assertFalse($this->_object->hasUnique());
	}
	
	public function testGetSource() : void
	{
		$this->assertEquals('php://memory:str('.\strlen(\file_get_contents(__DIR__.'/testdata.csv')).')', $this->_object->getSource());
	}
	
	public function testProvideOne() : void
	{
		$expected = ['Header' => 'Header 1', 'Column' => 'Value 1'];
		
		$this->assertEquals($expected, $this->_object->provideOne());
	}
	
	public function testProvideOneEmpty() : void
	{
		$provider = new CsvStringDataProvider('', true);
		$this->assertEquals([], $provider->provideOne());
	}
	
	public function testProvideAll() : void
	{
		$data = [
			['Header' => 'Header 1', 'Column' => 'Value 1'],
			['Header' => 'Header 2', 'Column' => 'Value 2'],
		];
		
		$this->assertEquals($data, $this->_object->provideAll());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new CsvStringDataProvider(
			\file_get_contents(__DIR__.'/testdata.csv'),
			true,
			',',
			'"',
			'\\',
			'ISO-8859-1',
			'UTF-8',
		);
	}
	
}
